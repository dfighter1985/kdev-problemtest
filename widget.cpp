/*
 * Copyright 2015 Laszlo Kis-Adam
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


#include "./widget.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>

#include <limits>

#include <interfaces/icore.h>
#include <interfaces/ilanguagecontroller.h>
#include <interfaces/iprojectcontroller.h>
#include <interfaces/iproject.h>
#include <shell/problem.h>
#include <shell/problemmodel.h>
#include <shell/problemmodelset.h>
#include <project/projectmodel.h>

Widget::Widget(QWidget *parent) :
QWidget(parent)
{
    m_idx = 1;

    QVBoxLayout *vl = new QVBoxLayout();
    QPushButton *addButton = new QPushButton("Add problem", this);
    QPushButton *addTreeButton = new QPushButton("Add problem tree", this);
    QPushButton *clearButton = new QPushButton("Clear problems", this);

    connect(addButton, &QPushButton::clicked, this, &Widget::onAddClicked);
    connect(addTreeButton, &QPushButton::clicked, this, &Widget::onAddTreeClicked);
    connect(clearButton, &QPushButton::clicked, this, &Widget::onClearClicked);
    vl->addWidget(addButton);
    vl->addWidget(addTreeButton);
    vl->addWidget(clearButton);
    vl->addSpacerItem(new QSpacerItem(1, 1, QSizePolicy::Minimum, QSizePolicy::Expanding));

    QHBoxLayout *hl = new QHBoxLayout();
    hl->addLayout(vl);
    hl->addSpacerItem(new QSpacerItem(1, 1, QSizePolicy::Expanding, QSizePolicy::Minimum));

    setLayout(hl);

    KDevelop::ILanguageController *lc =  KDevelop::ICore::self()->languageController();
    KDevelop::ProblemModelSet *pms = lc->problemModelSet();
    m_model = new KDevelop::ProblemModel(this);
    pms->addModel("Test", m_model);
}

Widget::~Widget()
{
    KDevelop::ILanguageController *lc =  KDevelop::ICore::self()->languageController();
    KDevelop::ProblemModelSet *pms = lc->problemModelSet();
    pms->removeModel("Test");
}

KDevelop::IProblem::Ptr Widget::generateProblem()
{
    QList<QString> files;
    KDevelop::IProjectController *pc = KDevelop::ICore::self()->projectController();
    foreach(const KDevelop::IProject* project, pc->projects()) {
        foreach (KDevelop::ProjectFileItem* file, project->projectItem()->fileList()) {
            files.push_back(file->indexedPath().str());
        }
    }

    int idx = 0;
    if(files.count() > 0)
        idx = rand() % files.count();

    KDevelop::DetectedProblem *p = new KDevelop::DetectedProblem();
    QString msg = "yada";
    msg += QString::number(m_idx++);
    p->setDescription(msg);

    KDevelop::DocumentRange range;
    if(files.count() == 0 )
        range.document = KDevelop::IndexedString("/just/a/bogus/path/yada.cpp");
    else
        range.document = KDevelop::IndexedString(files[idx]);

    p->setFinalLocation(range);
    p->setSource(KDevelop::IProblem::Plugin);
    p->setSeverity(KDevelop::IProblem::Severity(rand() % (KDevelop::IProblem::Hint + 1)));

    return KDevelop::IProblem::Ptr(p);
}

void Widget::onAddClicked(bool checked)
{
    Q_UNUSED(checked);

    KDevelop::IProblem::Ptr p = generateProblem();

    m_model->addProblem(p);
}

void Widget::onAddTreeClicked(bool checked)
{
    Q_UNUSED(checked);

    KDevelop::IProblem::Ptr p = generateProblem();

    KDevelop::IProblem::Ptr p2(new KDevelop::DetectedProblem());
    KDevelop::IProblem::Ptr p3(new KDevelop::DetectedProblem());
    KDevelop::IProblem::Ptr p4(new KDevelop::DetectedProblem());

    p2->setDescription(QStringLiteral("P2"));
    p2->setFinalLocation(p->finalLocation());
    p3->setDescription(QStringLiteral("P3"));
    p3->setFinalLocation(p->finalLocation());

    p4->setDescription(QStringLiteral("P4"));
    p4->setFinalLocation(p->finalLocation());

    p->addDiagnostic(p2);
    p3->addDiagnostic(p4);
    p->addDiagnostic(p3);

    m_model->addProblem(p);
}

void Widget::onClearClicked(bool checked)
{
    Q_UNUSED(checked);
    m_model->clearProblems();
}


/*
 * Copyright 2015 Laszlo Kis-Adam
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <interfaces/iproblem.h>

namespace KDevelop
{
class ProblemModel;
}

class Widget : public QWidget
{
    Q_OBJECT
public:
    Widget(QWidget *parent = NULL);
    ~Widget();

private Q_SLOTS:
    void onAddClicked(bool checked);
    void onAddTreeClicked(bool checked);
    void onClearClicked(bool checked);

private:
    KDevelop::IProblem::Ptr generateProblem();

    KDevelop::ProblemModel *m_model;
    int m_idx;
};

#endif

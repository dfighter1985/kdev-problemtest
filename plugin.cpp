/*
 * Copyright 2015 Laszlo Kis-Adam
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


#include <kpluginfactory.h>
#include <kpluginloader.h>

#include <interfaces/icore.h>
#include <interfaces/iuicontroller.h>
#include <interfaces/iplugincontroller.h>

#include "plugin.h"
#include "widget.h"

using namespace KDevelop;

K_PLUGIN_FACTORY_WITH_JSON(ProblemTestFactory, "kdevproblemtest.json",  registerPlugin<Plugin>();)

class WidgetFactory : public KDevelop::IToolViewFactory
{
public:
    WidgetFactory(Plugin* plugin)
        : m_plugin(plugin) {
    }

    virtual QWidget* create(QWidget *parent = NULL) {
        return new Widget(parent);
    }

    virtual Qt::DockWidgetArea defaultPosition() {
        return Qt::BottomDockWidgetArea;
    }

    virtual QString id() const {
        return "org.kdevelop.ProblemTestView";
    }

private:
    Plugin* m_plugin;
};

Plugin::Plugin(QObject *parent, const QVariantList&)
    : IPlugin("kdevproblemtest", parent)
    , m_factory(new WidgetFactory(this))
{
    setXMLFile("kdevproblemtest.rc");
    core()->uiController()->addToolView(i18n("ProblemTest"), m_factory);
}

Plugin::~Plugin()
{
}

void Plugin::unload()
{
    core()->uiController()->removeToolView(m_factory);
}

#include "plugin.moc"
